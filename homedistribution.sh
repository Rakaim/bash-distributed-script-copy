#!/bin/sh
#Author: Kurt Bomya

###################
### STATIC VARS ###
###################

server1=serverName1
server2=serverName2
serverx=serverName3


function deleteDirectory #Expects: 1 Machine 2 Username
{
	ssh $1 "chmod 700 /home/$2/*"
	ssh $1 "rm -rf /home/$2/*"
}

function loadDirectory #Expects: 1 Machine 2 Username
{
	scp -r /home/$2/homedistribution/newhome/* $1:/home/$2/
}

#Any special permissions after the copy is complete should be set here.
function updatePermission #Expects: 1 Machine 2 Username
{
	# Example:
	# ssh $1 "chmod 400 /home/$2/myfavorite.txt" 2> /dev/null
}

function newhome #Expects: 1 Machine 2 Username
{
	echo "Building a new home for $2 on $1"
	deleteDirectory $1 $2
	loadDirectory $1 $2
	updatePermission $1 $2
}

newhome #server1 service_account1
newhome #server2 service_account2
newhome #serverx service_accountx
