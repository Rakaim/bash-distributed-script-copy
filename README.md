# Bash Distributed Home
Files contained in the directory 'newhome' will be scp'd to all remote home directories to the servers specified in 'homedistribution.sh.

## Usage:
Replace the target service account with the ID that you're planning on using.
Update the server list at the top of the 'homestribution.sh' script with your target servers.

## Notes:
:warning: All contents in those remote home directories not beginning with a dot "." will be deleted before new files are pushed out.